# Mediko
An app that will allow nurses, doctors, pediatrician and other healthcare practitioners to signup to an app where they will be alerted in case someone needs a medical assistance in nearby locations. This will be helpful most specially when ambulances are not accessible to remote areas program developed as an entry for [GDG Android Masters] (http://androidmasters.gdgph.org/) Android App Development


# Project

Download on Google Play:

<a href="https://play.google.com/store/apps/details?id=edu.ucuccs.mediko" target="_blank">
  <img alt="Get it on Google Play"
       src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" height="60"/>
</a>

Having the sample project installed is a good way to be notified of new releases. Although Watching this 
repository will allow GitHub to email you whenever I publish a release.
